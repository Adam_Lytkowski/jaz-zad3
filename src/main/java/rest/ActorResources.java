package rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Actor;
import domain.Comment;
import domain.Film;
import domain.services.*;

@Path("/actor")
public class ActorResources {

	private ActorService as = new ActorService();
	private FilmService fs = new FilmService();
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getAllActors(){
List<String> actors = new ArrayList();
		
		for(Actor a : as.getAll()){
			actors.add(a.getName() + " " + a.getSurname() + " ID:" + a.getId());
		}
		return actors;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Add(Actor actor){
		as.add(actor);
		return Response.ok(actor.getId()).build();
	}
	
	@POST
	@Path("/{actorId}/films/{filmId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response AssignFilmToActor(@PathParam("actorId") int actorId, @PathParam("filmId") int filmId){
		Actor result1 = as.getActor(actorId); 
		if(result1.getFilms()==null)
		    	result1.setFilms(new ArrayList<Integer>());
		
		Film result2 = fs.getFilm(filmId); 
		if(result2.getActors()==null)
		    	result2.setActors(new ArrayList<Integer>());
		
	this.as.getActorsFilms(actorId).add(this.fs.getFilm(filmId).getId());
	this.fs.getFilmsActors(filmId).add(this.as.getActor(filmId).getId());
	
	return Response.ok().build();
	}
	
	 @GET
		@Path("/{actorId}")
		@Produces(MediaType.APPLICATION_JSON)
		public Actor getActorInfo(@PathParam("actorId") int actorId){
	           return as.getActor(actorId);       
		}
	
	@GET
	@Path("/{actorId}/films")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getActorsFilms(@PathParam("actorId") int actorId){
		Actor result = as.getActor(actorId); 
		if(result.getFilms()==null)
		    	result.setFilms(new ArrayList<Integer>());
		
		List<String> films =new ArrayList<String>();
        for(int i=0;i<as.getActorsFilms(actorId).size();i++){
            
        	films.add(fs.getFilm(as.getActorsFilms(actorId).get(i)).getName());
            
        }
        return films;
	
	}
}
