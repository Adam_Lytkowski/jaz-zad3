package domain.services;

import java.util.ArrayList;
import java.util.List;

import domain.Actor;
import domain.Comment;
import domain.Film;
import domain.services.FilmService;

public class ActorService {
	private static List<Actor> db = new ArrayList<Actor>();	
	private FilmService fs = new FilmService();
	
	
	static{
		Actor KitH = new Actor();
		KitH.setId(0);
		KitH.setName("Kit");
		KitH.setSurname("Harrington");
		
		Actor EmiC = new Actor();
		EmiC.setId(1);
		EmiC.setName("Emilia");
		EmiC.setSurname("Clarke");
		
		Actor MaiW = new Actor();
		MaiW.setId(2);
		MaiW.setName("Maisie");
		MaiW.setSurname("Williams");
		
		Actor SopT = new Actor();
		SopT.setId(3);
		SopT.setName("Sophie");
		SopT.setSurname("Turner");
		
		Actor LenH = new Actor();
		LenH.setId(4);
		LenH.setName("Lena");
		LenH.setSurname("Hedey");
		
		Actor IsaH = new Actor();
		IsaH.setId(5);
		IsaH.setName("Isaac");
		IsaH.setSurname("Hempstead-Wright");
		
		db.add(KitH);
		db.add(EmiC);
		db.add(MaiW);
		db.add(SopT);
		db.add(LenH);
		db.add(IsaH);
	}
	
	public List<Actor> getAll(){
		return db;
	}
	
	 public Actor getActor(int id){
			return db.get(id);
		}
	
	public void add(Actor a){
		a.setId(db.size());
		db.add(a);
	}
	
	
	public List<Integer> getActorsFilms(int actorId){
		return db.get(actorId).getFilms();
	}
}
