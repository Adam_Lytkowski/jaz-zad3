package domain;

import java.util.ArrayList;
import java.util.List;

public class Film {
private int id;
private String name;
private String description;
private List<Comment> comments;
private List<Integer> rating;
private List<Integer> actors;

public List<Integer> getActors() {
	return actors;
}
public void setActors(List<Integer> actors) {
	this.actors = actors;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public List<Comment> getComments() {
	return comments;
}
public void setComments(List<Comment> comments) {
	this.comments = comments;
}
public List<Integer> getRating() {
	return rating;
}
public void setRating(List<Integer> rating) {
	this.rating = rating;
}


}
