package rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Actor;
import domain.Comment;
import domain.Film;
import domain.services.ActorService;
import domain.services.FilmService;

@Path("/film")
public class FilmResources {
	private ActorService as = new ActorService();
	private FilmService fs = new FilmService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getAllFilms(){
		List<String> films = new ArrayList();
		
		for(Film f : fs.getAll()){
			films.add(f.getName() + " ID:" + f.getId());
		}
		return films;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Add(Film film){
		fs.add(film);
		return Response.ok(film.getId()).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") int id){
	Film result = fs.getFilm(id);
	if(result==null){
		return Response.status(404).build();
	}
	return Response.ok(result).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id,Film f){
		Film result = fs.getFilm(id);
		if(result==null)
			return Response.status(404).build();
		f.setId(id);
		fs.update(f);
		return Response.ok().build();
	}
	
	@POST
	@Path("/{filmId}/{r}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response rate(@PathParam("filmId") int id,@PathParam("r") int r){
		if(r<1 || r>10)
			return Response.status(404).build();
		
		Film result = fs.getFilm(id);
	    if(result==null)
	      return null;
	    if(result.getRating()==null)
	        result.setRating(new ArrayList<Integer>());
	    result.getRating().add(r);
		
		return Response.ok().build();
	}
	
	@GET
	@Path("/{filmId}/rating")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRating(@PathParam("filmId") int id){
		Film result = fs.getFilm(id);
	    if(result==null)
	      return null;
	    if(result.getRating()==null)
	        result.setRating(new ArrayList<Integer>());
	    
	    double sum=0;
	    if(!result.getRating().isEmpty()){
	    	for(int r : result.getRating()){
	    		sum+=r;
	    	}	
	    	double average = sum/result.getRating().size();
	    	 return Response.ok(average).build();
	    }else{
	    return Response.ok().build();
	}}
	
	
	@GET
	@Path("/{filmId}/comments")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getAllComments(@PathParam("filmId") int id){
		Film result = fs.getFilm(id);
    if(result==null)
      return null;
    if(result.getComments()==null)
    result.setComments(new ArrayList<Comment>());
    return result.getComments();
	}
	
	@POST
	@Path("/{filmId}/comments")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response AddComment(@PathParam("filmId") int id, Comment comment){
		Film result = fs.getFilm(id);
		if(result==null)
			return Response.status(404).build();
    if(result.getComments()==null)
    	result.setComments(new ArrayList<Comment>());
    fs.addComment(id, comment);
    return Response.ok().build();  
  }
	
	@DELETE
	@Path("/{filmId}/comments/{commentId}")
	public Response deleteComment(@PathParam("filmId") int id,@PathParam("commentId") int commentId){
		Film result = fs.getFilm(id);
		if(result==null)
			return Response.status(404).build();
		fs.deleteComment(id,commentId);
	    return Response.ok().build();
	}
	
	@GET
	@Path("/{filmId}/actors")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getFilmsActors(@PathParam("filmId") int filmId){
		Film result = fs.getFilm(filmId); 
		if(result.getActors()==null)
		    	result.setActors(new ArrayList<Integer>());
		
			 List<String> actors =new ArrayList<String>();
         for(int i=0;i<fs.getFilmsActors(filmId).size();i++){
             String name =as.getActor(fs.getFilmsActors(filmId).get(i)).getName()+ " "+as.getActor(fs.getFilmsActors(filmId).get(i)).getSurname();
             actors.add(name);
             
         }
         
         return actors;
	
	}
}
