package domain.services;

import java.util.ArrayList;
import java.util.List;

import domain.Actor;
import domain.Comment;
import domain.Film;


public class FilmService {
	private static List<Film> db = new ArrayList<Film>();


	
	static{
		Film GoT = new Film();
		GoT.setId(0);
		GoT.setName("Game of Thrones");
		GoT.setDescription("KING IN THE NORTH");
		
		Film Wolf = new Film();
		Wolf.setId(1);
		Wolf.setName("The Wolf of Wall Street");
		Wolf.setDescription("wolfie");
		
		Film Titanic = new Film();
		Titanic.setId(2);
		Titanic.setName("Titanic");
		Titanic.setDescription("come baaaack");
		
		db.add(GoT);
		db.add(Wolf);
		db.add(Titanic);
		
	}
	
	public List<Film> getAll(){
		return db;
	}
	
	public Film getFilm(int id){
		 return db.get(id);
	}
	
	public void add(Film f){
		f.setId(db.size());
		db.add(f);
	}
	
	public void update(Film film){
		for(Film f : db){
			if(f.getId()==film.getId()){
				f.setName(film.getName());
				f.setDescription(film.getDescription());
			}
		}
	}
	
	
	public void addComment(int id, Comment comment){
		for(Film f : db){
			if(f.getId()==id){
				comment.setId(f.getComments().size());
				f.getComments().add(comment);
			}
			}
	}
	
	public void deleteComment(int id, int commentId){
		int i=0;
		for(Film f : db){
			if(f.getId()==id){
				f.getComments().remove(commentId);
				for(Comment c : f.getComments()){
					c.setId(i);
					i++;
				}
		
			}
			}
	}
	
public List<Integer> getFilmsActors(int actorId){
		
		return db.get(actorId).getActors();
        }
	
	
	public double getRating(int id){
		int w = 0;
		for(Film f : db){
			if(f.getId()==id){
				for(int g : f.getRating()){
				w+=g;	
				}
				return w/f.getRating().size();
			}		
		}
		return (Double) null;
	}
}
